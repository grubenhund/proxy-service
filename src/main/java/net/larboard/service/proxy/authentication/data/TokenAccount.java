package net.larboard.service.proxy.authentication.data;

import io.jsonwebtoken.Claims;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class TokenAccount {
    public TokenAccount() {}

    public TokenAccount(Claims claims) {
        username = (String)claims.get("sub");
        admin = false;

        if(claims.get("t3_groups") != null) {
            List<String> groups = (List<String>) claims.get("t3_groups");

            admin = groups.contains("2");
        }
    }

    private String username;

    private boolean admin;
}
